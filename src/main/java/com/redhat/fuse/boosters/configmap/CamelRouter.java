/**
 *  Copyright 2005-2016 Red Hat, Inc.
 *
 *  Red Hat licenses this file to you under the Apache License, version
 *  2.0 (the "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied.  See the License for the specific language governing
 *  permissions and limitations under the License.
 */
package com.redhat.fuse.boosters.configmap;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.KafkaManualCommit;
import org.springframework.stereotype.Component;

@Component
public class CamelRouter extends RouteBuilder {
	
    @Override
    public void configure() throws Exception {

		onException(Exception.class)
            .maximumRedeliveries("{{maximumRedeliveries}}")
            .redeliveryDelay("{{redeliveryDelay}}")
            .onRedelivery(exchange -> {
                int retryCount = exchange.getIn().getHeader(Exchange.REDELIVERY_COUNTER, Integer.class);
                log.info("#### Redelivery {} Intento ####" , retryCount);
			})
            .retryAttemptedLogLevel(LoggingLevel.INFO);

		//FROM INCOMING TOPIC TO GOOGLE PUBSUB
		from("kafka://{{kafka.in.topic}}?brokers={{kafka.in.brokers.host}}:{{kafka.in.brokers.port}}&groupId={{kafka.in.groupId}}&fetchWaitMaxMs={{kafka.in.fetchWaitMaxMs}}&fetchMinBytes={{kafka.in.fetchMinBytes}}&fetchMaxBytes={{kafka.in.fetchMaxBytes}}&maxPartitionFetchBytes={{kafka.in.maxPartitionFetchBytes}}&autoCommitEnable={{kafka.in.autoCommitEnable}}&autoCommitIntervalMs={{kafka.in.autoCommitIntervalMs}}&sessionTimeoutMs={{kafka.in.sessionTimeoutMs}}&heartbeatIntervalMs={{kafka.in.heartbeatIntervalMs}}&autoOffsetReset={{kafka.in.autoOffsetReset}}&maxPollIntervalMs={{kafka.in.maxPollIntervalMs}}&maxPollRecords={{kafka.in.maxPollRecords}}&allowManualCommit={{kafka.in.allowManualCommit}}")
		.routeId("route_from_in_topic_to_pubsub")
			.log(LoggingLevel.INFO, "#### Inicio ####")
			.log(LoggingLevel.INFO, "in.headers: ${in.headers}")
			//.log(LoggingLevel.INFO, "body: ${body}")
			//.log(LoggingLevel.INFO, "mivariable: {{mivariable}}")
			.to("google-pubsub://{{pubsub.projectId}}:{{pubsub.destinationName}}")
			//.throwException(new Exception("Excepción Forzada!!!!"))
			.log(LoggingLevel.INFO, "Antes de Comitiar")
			.process(exchange -> {
				KafkaManualCommit manual = exchange.getIn().getHeader(KafkaConstants.MANUAL_COMMIT, KafkaManualCommit.class);
				if (manual != null) {
					manual.commitSync();
				}
			})
			.log(LoggingLevel.INFO, "Despues de Comitiar")
			.log(LoggingLevel.INFO, "#### Fin ####");
    }

}